package controllers;

import java.io.IOException;

import javax.faces.bean.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import beans.User;

@ManagedBean 
@ViewScoped
public class FormController {
	
	public String onSubmit(User user) {
		System.out.println(user.getFirstName());
		System.out.println(user.getLastName());
		
		return "TestResponse.xhtml";
		
	}
	public String onFlash(User user){
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		System.out.println(FacesContext.getCurrentInstance().getClientIdsWithMessages());
		getRequestMap();
		return "TestResponse.xhtml?faces-redirect=true";
		
	}
	
	public String getRequestMap() {
		return "TestResponse.xhtml";
		
	}

}
