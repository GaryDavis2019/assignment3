package beans;
import javax.faces.bean.*;

@ManagedBean 
@ViewScoped
public class User {
	String firstName;
	String lastName;

	/** Constructors */ 
	public User(String FirstName, String LastName) { 
		firstName = FirstName;
		lastName = LastName;
	}
	
	public User() { 
		firstName = "Gary";
		lastName = "Davis";
	}
	
	//First Name getter/setter	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String FirstName) {
		firstName = FirstName;
	}
	
	//Last Name getter/setter
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String LastName) {
		lastName = LastName;
	}

}
